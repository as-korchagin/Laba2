#include <iostream>
#include <cstring>
#include "Fraction.h"

using namespace std;

bool isDouble(char *);

int main() {
    Fraction a;
    Fraction b, y;
    char *left = new char[50];
    char *right = new char[50];
    cout << "Введите 1-ю дробь: ";
    cin.getline(left, 50);
    cout << "Введите 2-ю дробь: ";
    cin.getline(right, 50);
    if (isDouble(left)) {
        a = Fraction(atof(left));
    } else {
        a = Fraction(left);
    }
    if (isDouble(right)) {
        b = Fraction(atof(right));
    } else {
        b = Fraction(right);
    }
    Fraction c = a + b;
    cout << c;
//    y = a + b;
//    cout << "y=" << y << endl;
//    y += x;
//    y += 3.14 / 2;
//    cout << "f=" << f << endl;
//    y = x + dbl;
//    cout << "y=" << y << endl;
//    y = dbl + y;
//    cout << "y=" << y << endl;
//    y += dbl;
//    cout << "y=" << y << endl;
//    int i = 5;
//    y += i;
//    cout << "y=" << y << endl;
//    y = i + x;
//    cout << "y=" << y << endl;
//    y = x + i;
//    cout << "y=" << y << endl;
//    y += dbl + i + x;
//    cout << "y=" << y << endl;

    delete[] left;
    delete[] right;

}

bool isDouble(char *input) {
    // Проверяет введенную дробь на double / not double
    for (int i = 0; i < strlen(input); i++) {
        if (input[i] == '.') {
            return true;
        }
    }
    return false;
}