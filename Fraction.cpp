//
// Created by andrey on 17.11.16.
//

#include <cstring>
#include <cmath>
#include "Fraction.h"

#define N_DEC 4

using namespace std;

int Fraction::fraction_count = 0;

Fraction::Fraction() {
    //Конструкстор класса Fraction по умолчанию
//    cout << ++fraction_count << endl;
    intPart = 0;
    numerator = 0;
    denominator = 0;
}

Fraction::Fraction(bool negative, int intPart, int numerator, int denominator, bool simplify) {
    //Специальный конструктор типа Fraction с 5 параметрами
//    cout << ++fraction_count << endl;
    this->intPart = intPart;
    this->negative = negative;
    this->numerator = numerator;
    this->denominator = denominator;
    if (simplify) {
        Fraction::simplifyFraction();
    }
};

Fraction::Fraction(char *input) {
    //Специальный конструктор класса Fraction, составляющий дробь из строки
//    cout << ++fraction_count << endl;
    intPart = 0;
    count = 0;
    char *s = strtok(input, delimiter);
    while (s != NULL) {
        count++;
        switch (count) {
            case 1 : {
                intPart = atoi(s);
                break;
            }
            case 2: {
                numerator = atoi(s);
                break;
            }
            case 3: {
                denominator = atoi(s);
                break;
            }
            default: {
                break;
            }
        };
        s = strtok(NULL, delimiter);
    };
    switch (count) {
        case 1: {
            denominator = 1;
            numerator = 0;
            break;
        }
        case 2: {
            denominator = numerator;
            numerator = intPart;
            intPart = 0;
        }
        default: {
            break;
        }
    }
    if ((numerator < 0) || (intPart < 0)) {
        negative = true;
    }
    count = 0;
    Fraction::simplifyFraction();
}

void Fraction::simplifyFraction() {
    //Упрощение дроби
    for (int i = 0; i < 4; i++) {
        bool stop = false;
        while (!stop) {
            if ((numerator % divisors[i] == 0) && (numerator != 0)) {
                if (denominator % divisors[i] == 0) {
                    numerator /= divisors[i];
                    denominator /= divisors[i];
                } else {
                    stop = true;
                    continue;
                }
            } else {
                stop = true;
                continue;
            }
        }
    }
    if (numerator > 0) {
        while (numerator > denominator) {
            if ((numerator - denominator) > 0) {
                numerator -= denominator;
                intPart++;
                if ((numerator - denominator) == 0) {
                    numerator = 0;
                    denominator = 0;
                    intPart += 1;
                }
            }
        }
    } else {
        while (abs(numerator) > denominator) {
            if ((abs(numerator) - denominator) > 0) {
                numerator += denominator;
                intPart--;
                if ((abs(numerator) - denominator) == 0) {
                    numerator = 0;
                    denominator = 0;
                    intPart -= 1;
                }
            }
        }
//        numerator = abs(numerator);
    }
}

Fraction::~Fraction() {
    //Деструктор класса Fraction
//    cout << --fraction_count << endl;
}

Fraction::Fraction(double dbl) {
    //Специальный конструктор типа Fraction, преобразующий double в дробь
//    cout << ++fraction_count << endl;
//    cout << "fraction double" << b << endl;
    this->negative = dbl < 0;
    this->intPart = (int) dbl;
    this->numerator = (int) abs(((dbl - this->intPart) * pow(10, N_DEC - 1)));
    this->denominator = pow(10, N_DEC - 1);
    simplifyFraction();
}

Fraction Fraction::operator+(const Fraction &rvalue) {
    //Перегрузка оператора + с одним параметром Fraction
    Fraction right, left, f;
    right = makeIrregularFraction(*this);
    left = makeIrregularFraction(rvalue);
    int tempNum;
    if (right.getDenominator() * left.getDenominator() == 1) {
        tempNum = (left.intPart != 0 ? left.intPart * left.numerator : left.numerator) +
                  (right.intPart != 0 ? right.intPart * right.numerator : right.numerator);
        f = Fraction(tempNum < 0, 0, tempNum, 1, true);
    } else {
        tempNum = (right.numerator * left.denominator + left.numerator * right.denominator);
        f = Fraction(tempNum < 0, 0, tempNum, (left.denominator * right.denominator), true);
    }

    return f;
}

Fraction &Fraction::operator=(const Fraction &rvalue) {
    //Перегрузка оператора = с одним параметром
    intPart = rvalue.intPart;
    negative = rvalue.negative;
    numerator = rvalue.numerator;
    denominator = rvalue.denominator;
    return *this;
}


ostream &operator<<(ostream &stream, Fraction &fraction) {
    //Перегрузка оператора <<
    fraction.simplifyFraction();
    if (fraction.getIntPart() == 0) {
        if (fraction.getDenominator() == 1) {
            stream << fraction.getNumerator() << endl;
        } else if (fraction.getNumerator() == 0) {
            stream << 0 << endl;
        } else {
            stream << fraction.getNumerator() << '/' << fraction.getDenominator() << endl;
        }
    } else {
        if (fraction.getNumerator() == 0) {
            stream << fraction.getIntPart() << endl;
        } else {
            stream << fraction.getIntPart() << ' ' << abs(fraction.getNumerator()) << '/' << fraction.getDenominator()
                   << endl;
        }
    }
    return stream;
}

Fraction Fraction::operator*(const Fraction &fraction) {
    //Перегрузка оператора * с одним параметром
    Fraction left = makeIrregularFraction(*this);
    Fraction right = makeIrregularFraction(fraction);
    Fraction f = Fraction(left.numerator * right.numerator < 0, 0, left.numerator * right.numerator,
                          left.denominator * right.denominator, true);
    return f;
}

Fraction Fraction::operator+(double rvalue) {
    //Перегрузка оператора + с одним параметром double
    Fraction fr(rvalue);
    Fraction result = *this + rvalue;
    return result;
}

Fraction operator+(const double rvalue, const Fraction &lvalue) {
    //Перегрузка оператора + с двумя параметрами (double, Fraction)
    Fraction left = Fraction(rvalue);
    Fraction right = Fraction(lvalue);
    left += right;
    return left;
}

Fraction Fraction::makeIrregularFraction(const Fraction &fraction) {
    //Преобразование правильной дроби в неправильную
    int tempNum;
    if (fraction.negative) {
        if (fraction.intPart != 0) {
            tempNum = (-1) * (abs(fraction.numerator) +
                              ((fraction.denominator == 0 ? 1 : abs(fraction.denominator)) * abs(fraction.intPart)));
        } else {
            tempNum = (-1) * abs(fraction.numerator);
        }
    } else {
        if (fraction.intPart != 0) {
            tempNum = (fraction.numerator +
                       ((fraction.denominator == 0 ? 1 : fraction.denominator) * fraction.intPart));
        } else {
            tempNum = fraction.numerator;
        }
    }
    Fraction f(fraction.negative, 0, tempNum,
               (fraction.denominator == 0 ? 1 : fraction.denominator), false);
    return f;
}

void Fraction::operator+=(const Fraction &rvalue) {
    //Перегрузка оператора +=
    Fraction left, right;
    left = makeIrregularFraction(*this);
    right = makeIrregularFraction(rvalue);
    int tempNum = right.numerator * left.denominator + left.numerator * right.denominator;
    Fraction f(tempNum < 0, 0, tempNum, (left.denominator * right.denominator), true);
    *this = f;
}