#include "iostream"

using namespace std;

class Fraction {
private:
    int intPart;
    int numerator;
    int denominator;
    int divisors[4] = {2, 3, 5, 7};
    char delimiter[3] = " /";
    int count;
    bool negative = false;
    char dot = '.';
public:

    static int fraction_count;

    Fraction(char *);

    Fraction(double);

    Fraction(bool, int, int, int, bool);

    Fraction();

    ~Fraction();

    void simplifyFraction();

    Fraction operator+(const Fraction &);

    Fraction operator+(double);

    friend Fraction operator+(const double, const Fraction&);

    Fraction &operator=(const Fraction &);

    bool getNegative() {
        return negative;
    }

    int getIntPart() {
        return intPart;
    }

    int getNumerator() {
        return numerator;
    }

    int getDenominator() {
        return denominator;
    }

    Fraction operator*(const Fraction &);

    Fraction makeIrregularFraction(const Fraction &);

    void operator+=(const Fraction &);
};

ostream &operator<<(ostream &, Fraction &);

